using System;
namespace matematica
{
    class Program 
    {
        static void Main(string[] args) 
        {
            Rettangolo ret1 = new Rettangolo();
            Console.WriteLine(Shape.contatore);
            //Console.WriteLine(ret1.area(15, 40));
            // Console.WriteLine(ret1.CalcolaMedia(3, 7, 8, 9, 5));
            Cerchio cerc1 = new Cerchio();
            Console.WriteLine(Shape.contatore);
            //Console.WriteLine(cerc1.area(10, 3.14));
            //Console.WriteLine(cerc1.circonferenza(10, 3.14));
            Trapezio trap1 = new Trapezio();
            Console.WriteLine(Shape.contatore);
            //Console.WriteLine(trap1.area(5, 8, 3));
            Teorema tr1 = new Teorema();
            Console.WriteLine(Shape.contatore);
            //Console.WriteLine(tr1.TeoremaPitagora(3, 4));
            Shape shape1 = new Shape();
            Console.WriteLine(Shape.contatore);
            Ricorsione fatt = new Ricorsione();
            Console.WriteLine(Shape.contatore);
            //Console.WriteLine(fatt.fattoriale(13));
            Omnia omnia1 = new Omnia();
            Console.WriteLine(Shape.contatore);
            // omnia1.area();
        }
    }
    class Shape
    {
        public static int contatore;
        public Shape() { contatore++; }
        public int x { get; set; }
        public int y { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int area(int Base, int altezza) { return (Base * altezza); }
        public double CalcolaMedia(params double[] array)
        {
            double media = 0;
            for (int i = 0; i < array.Length; i++)
            {
                media += array[i];
            }
            return media / array.Length;
        }
    }
    class Rettangolo : Shape
    {
        public int area(int Base) { return (Base * Base); } //è un overload della classe padre. abbiamo infatti due int area, qua dice che se nel writeline abbiamo un solo parametro, cioè base*base, prendo questa qui. se ce ne sono due, cioè base*altezza, prendiamo quello della classe shape.
    }
    class Omnia : Shape
    {
        public void area()
        {
            Console.WriteLine("inserisci almeno un parametro");
        }
    }
    class Cerchio : Shape
    {
        public double area(int raggio, double piGreco) { return (raggio * raggio * piGreco); }
        public double circonferenza(int raggio, double piGreco) { return (raggio * 2 * piGreco); }
    }
    class Trapezio : Shape
    {
        public double area(int baseMaggiore, int baseMinore, int altezza)
        {
            return (((baseMaggiore + baseMinore) * altezza) / 2);
        }
    }
    class Teorema
    {
        public double TeoremaPitagora(double cateto1, double cateto2)
        {
            return Math.Sqrt(Math.Pow(cateto1, 2) + (cateto2 * cateto2));
        }
    }
    class Ricorsione
    {
        public int fattoriale(int numero)
        {
            if (numero <= 1)
            {
                return 1;
            }
            else
            {
                return numero * fattoriale(numero - 1);
            }
        }
    }
}
//i costruttori sono metodi particolari che inizializzano le classi. è sempre un metodo pubblico e per convenzione ha lo stesso nome della classe
